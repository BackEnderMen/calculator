import React from 'react';
import {Text, StyleSheet, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';

const FooterComponent = ({
                             onClick,
                             text,
                             btnText,
                         }) => {
    const handleClick = (e) => {
        e.preventDefault();
        onClick && onClick();
    };

    return(
        <View style={styles.signUnTextCont}>
            <Text style={styles.signUpText}>{text}</Text>
            <Text style={styles.signUpButton} onPress={handleClick}>{btnText}</Text>
        </View>
    )
}


const styles = StyleSheet.create({

    signUnTextCont: {
        justifyContent: 'center',
        paddingVertical: 16,
        flexDirection: 'row',
    },
    signUpText: {
        color:'rgba(255,255,255,0.6)',
        fontSize: 16,
    },
    signUpButton: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '500',
    },

})

FooterComponent.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string.isRequired,
    btnText: PropTypes.string.isRequired,
};

FooterComponent.defaultProps = {
    onClick: () => {},
}

export default FooterComponent
