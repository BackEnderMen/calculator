import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

const ButtonComponent = ({
    onClick,
    value,
    styleVariant,
}) => {
    const handleClick = (e) => {
        e.preventDefault();
        onClick && onClick();
    };

    return(
        <TouchableOpacity
            style={styleVariant === 'number' ? styles.number : styleVariant === 'char'? styles.char : styles.exiit}
            onPress={handleClick}
            activeOpacity={0.7}
        >
            <Text style = {styleVariant === 'number' ? styles.numberText : styleVariant === 'char'? styles.charText : styles.exitText} > { value } </Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    number: {
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#299187',
        margin: 3,
    },
    char: {
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#355f82',
        margin: 3,
    },
    numberText: {
        color: '#fff',
        fontWeight: 'bold',
    },
    charText: {
        color: '#fff',
        fontWeight: 'bold',
    },
    exiit: {
/*
        some css here
*/
    },
    exitText: {
        color: '#3ebdaa',
        fontWeight: 'bold',
    },

})

ButtonComponent.propTypes = {
    onClick: PropTypes.func,
    value: PropTypes.string.isRequired,
    styleVariant: PropTypes.string,
};

ButtonComponent.defaultProps = {
    onClick: () => {},
    styleVariant: 'number',
}

export default ButtonComponent
