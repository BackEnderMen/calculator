import React, { useState } from 'react'
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Footer from '../components/Footer';

const SignInScreen = ({navigation}) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = async () => {
        try {
            let data = await AsyncStorage.getItem(email);
            data = JSON.parse(data)
            if(data===null) {
                alert('There no user with this email')
            }else if(data.password !== password) {
                alert('Incorrect password')
            }else {
                // alert('Ok')
                await AsyncStorage.setItem('user', data.name)
                navigation.navigate('Calculator');
            }
        } catch (e) {
            alert(e)
        }
    }

    const signUp = () => {
        navigation.navigate('Registration');

    }

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Sign-in</Text>

            <TextInput
                style={styles.textInput}
                placeholder='Email'
                onChangeText = {(value) => setEmail(value)}
                underLineColorAndroid={'transparent'}
            />
            <TextInput
                style={styles.textInput}
                placeholder='Password'
                onChangeText = {(value) => setPassword(value)}
                secureTextEntry={true}
                underLineColorAndroid={'transparent'}
            />

            <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                <Text style={styles.btnText}>Sign-In</Text>
            </TouchableOpacity>
            <View style={styles.footer}>
                <Footer btnText={'SignUp'} text={'Don`t have an account yet?'} onClick={signUp}/>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#36485f',
        alignSelf: 'stretch',
        paddingLeft: 20,
        paddingRight: 20,
    },
    header: {
        fontSize: 24,
        color: '#fff',
        paddingBottom: 10,
        marginBottom: 40,
        borderBottomColor: '#199187',
        borderBottomWidth: 1,
    },
    textInput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 30,
        color: '#fff',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    button: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#59cbbd',
        marginTop: 30,
    },
    btnText: {
        color: '#fff',
        fontWeight: 'bold',
    },
    footer: {
        position: 'absolute',
        bottom: 0
    },

})

export default SignInScreen
