import React, { useState } from 'react'
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Footer from '../components/Footer'
const RegistrationScreen = ({navigation}) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const validateEmail = (val) => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(val)
    }
    const handleValidation = (e) => {
        if(name.length === 0){
            alert('Name field should have value')
            return false;
        }else if(!validateEmail(email)) {
            alert('Invalid email address')
            return false;
        }else if(password.length < 8) {
            alert('Password field should have at least 8 characters')
            return false;
        }
        return true;
    }
    const handleSubmit = async (e) => {
        if (handleValidation(e)) {
            try {
                // alert(name + " " + email + " " + password);
                await AsyncStorage.setItem(email, JSON.stringify({name, password}));
            } catch (e) {
                console.log(e)
            }
            navigation.navigate('SignIn');
        }
    }
    const signIn = () => {
        navigation.navigate('SignIn');
    }

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Registration</Text>

            <TextInput
                style={styles.textInput}
                placeholder='Name'
                onChangeText = {(value) => setName(value)}
                underLineColorAndroid={'transparent'}
            />
            <TextInput
                style={styles.textInput}
                placeholder='Email'
                onChangeText = {(value) => setEmail(value)}
                keyboardType={'email-address'}
                underLineColorAndroid={'transparent'}
            />
            <TextInput
                style={styles.textInput}
                placeholder='Password'
                onChangeText = {(value) => setPassword(value)}
                secureTextEntry={true}
                underLineColorAndroid={'transparent'}
            />

            <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                <Text style={styles.btnText}>Sign-Up</Text>
            </TouchableOpacity>
            <View style={styles.footer}>
                 <Footer btnText={'Sign-In'} text={'Already have an account?'} onClick={signIn}/>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#36485f',
        alignSelf: 'stretch',
        paddingLeft: 20,
        paddingRight: 20,
    },
    header: {
        fontSize: 24,
        color: '#fff',
        paddingBottom: 10,
        marginBottom: 40,
        borderBottomColor: '#199187',
        borderBottomWidth: 1,
    },
    textInput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 30,
        color: '#fff',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    button: {
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#59cbbd',
        marginTop: 30,
    },
    btnText: {
        color: '#fff',
        fontWeight: 'bold',
    },
    footer: {
        position: 'absolute',
        bottom: 0
    },

})

export default RegistrationScreen
