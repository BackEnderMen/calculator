import React, { useState, useEffect  } from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Button from '../components/Button';


const CalculatorScreen = ({navigation}) => {

    const [input, setInput] = useState('');

    let userName = '';

    // useEffect(()  => {
    //     userName = AsyncStorage.getItem('user')
    //     alert(JSON.stringify(user))
    // },[])

    const onExitClick = () => {
        navigation.navigate('Registration')
    }
    const onBackSpaceClick = () => {
        if(input === 'Infinity' || input === 'NaN'){
            setInput('')
        }else{
            setInput(input.slice(0, -1))
        }
    }
    const operations = [ '*', '/', '+', '-', '%', '^' ]
    const putPoint = () => {
        let str = input
        let flag = true
        for (let i = str.length; i >= 0; i--){
            if(str[i]==='.') {
                flag = false;
            }
            else if(operations.includes(str[i])){
                break
            }
        }
        if(flag){
            if(str.length===0 || operations.includes(str[str.length-1])){
                setInput(str+'0.')
            } else {
                setInput(str + '.')
            }
        }
    }
    const count = (str, ch) => {
        let counter = 0
        for(let i= 0;i<str.length;i++) {
            if(str[i]===ch)counter++
        }
        return counter
    }
    const onMultiplyClick = () => {
        let lastChar = input.slice(-1)
        let beforeLastChar = input[input.length-2]
        if(!operations.includes(lastChar) && input.length > 0){
            setInput(input + '*');
        }
        else if(operations.includes(lastChar) && input.length!==1 && !operations.includes(beforeLastChar)){
            setInput(input.slice(0, -1) + '*')
        }
    }
    const onDivideClick = () => {
        let lastChar = input.slice(-1)
        let beforeLastChar = input[input.length-2]
        if(!operations.includes(lastChar) && input.length > 0) {
            setInput(input + '/');
        }else if(operations.includes(lastChar) && input.length!==1 && !operations.includes(beforeLastChar)){
            setInput(input.slice(0, -1) + '/')
        }
    }
    const onPlusClick = () => {
        let lastChar = input.slice(-1)
        if(!operations.includes(lastChar) && input.length >0){
            setInput(input + '+');
        }else if (lastChar === '-' && input.length === 1) {
            setInput('');
        } else if(operations.includes(lastChar)) {
            setInput(input.slice(0, -1) + '+')
        }
    }
    const onMinusClick = () => {
        let lastChar = input.slice(-1)
        if(lastChar === '+'){
            setInput(input.slice(0, -1) + '-')
        } else if(lastChar !== '-') {
            setInput(input + '-');
        }
    }
    const onPowClick = () => {
        let lastChar = input.slice(-1)
        if(!operations.includes(lastChar) && input.length >0){
            setInput(input + '^');
        }else if(operations.includes(lastChar) && input.length!==1 ){
            setInput(input.slice(0, -1) + '^')
        }
    }
    const onPercentClick = () => {
        let lastChar = input.slice(-1)
        if(!operations.includes(lastChar) && input.length >0){
            setInput(input + '%');
        }else if(operations.includes(lastChar) && input.length!==1 ){
            setInput(input.slice(0, -1) + '%')
        }
    }
    const doOperations = (operation) => {
        let myInput = input;
        let result , counter = count(myInput, operation);
        while (counter--!==0){
            const indexOfOperation = myInput.indexOf(operation);
            let first = ''
            let second = ''
            let x, y;
            for (let i=indexOfOperation-1;i>=0;i--){
                if(myInput[i]==='-' || i === 0){
                    first = myInput.substring(i, indexOfOperation)
                    x = i
                    break;
                }else if(operations.includes(myInput[i])){
                    first = myInput.substring(i+1,indexOfOperation)
                    x = i + 1
                    break;
                }
            }
            for (let i = indexOfOperation+1; i < myInput.length; i++){
                if(operations.includes(myInput[i]) && myInput[i-1] !== operation ){
                    second = myInput.substring(indexOfOperation+1, i)
                    y = i
                    break;
                }else if(i === myInput.length-1){
                    second = myInput.substring(indexOfOperation+1, i+1)
                    y = i+1
                }
            }
            if(operation === '*' && first !=='' && second !== '') {
                result = parseFloat(first) * parseFloat(second)
            }else if(operation === '^' && first !=='' && second !== '') {
                result = Math.pow( parseFloat(first), parseFloat(second) )
            }else if(operation === '%' && first !=='' && second !== '') {
                result = (parseFloat(first) / 100) * parseFloat(second)
            }
            else if(operation === '/' && first !=='' && second !== '') {
                result = parseFloat(first) / parseFloat(second)
            }
            else if(operation === '+' && first !=='' && second !== '') {
                result = parseFloat(first) + parseFloat(second)
            }else if(operation === '-' && first !=='' && second !== '') {
                result = parseFloat(first) - parseFloat(second)
            }
            if(result===undefined)continue
            myInput = myInput.substring(0, x) + result + myInput.substring(y, myInput.length)
            setInput(myInput)
        }
    }


    const onEqualsClick = () => {
        doOperations('-')
        doOperations('+')
        doOperations('/')
        doOperations('*')
        doOperations('^')
        doOperations('%')
    }

    return(
        <View style={styles.container}>
            <View style={styles.exitBtn}>
                <Button value={'Exit'} styleVariant={'exit'} onClick={ onExitClick }/>
            </View>
            <Text style={styles.header}>{'Hello ' + userName}</Text>
            <TextInput
                style={styles.textInput}
                placeholder=''
                underLineColorAndroid={'transparent'}
                value={input}
                editable={false}
            />
            <View style = {styles.buttons}>
                <View style={styles.row}>
                    <Button value={'7'} styleVariant={'number'} onClick={ () => setInput(input+7) }/>
                    <Button value={'8'} styleVariant={'number'} onClick={ () => setInput(input+8) }/>
                    <Button value={'9'} styleVariant={'number'} onClick={ () => setInput(input+9) }/>
                    <Button value={'<-'} styleVariant={'char'} onClick={ onBackSpaceClick }/>
                    <Button value={'Delete'} styleVariant={'char'} onClick={ () => setInput('') }/>
                </View>
                <View style={styles.row}>
                    <Button value={'4'} styleVariant={'number'} onClick={ () => setInput(input+4) }/>
                    <Button value={'5'} styleVariant={'number'} onClick={ () => setInput(input+5) }/>
                    <Button value={'6'} styleVariant={'number'} onClick={ () => setInput(input+6) }/>
                    <Button value={'*'} styleVariant={'char'} onClick={ onMultiplyClick }/>
                    <Button value={'+'} styleVariant={'char'} onClick={ onPlusClick }/>
                </View>
                <View style={styles.row}>
                    <Button value={'1'} styleVariant={'number'} onClick={ () => setInput(input+1) }/>
                    <Button value={'2'} styleVariant={'number'} onClick={ () => setInput(input+2) }/>
                    <Button value={'3'} styleVariant={'number'} onClick={ () => setInput(input+3) }/>
                    <Button value={'/'} styleVariant={'char'} onClick={ onDivideClick }/>
                    <Button value={'-'} styleVariant={'char'} onClick={ onMinusClick }/>
                </View>
                <View style={styles.row}>
                    <Button value={'.'} styleVariant={'number'} onClick={ putPoint }/>
                    <Button value={'0'} styleVariant={'number'} onClick={ () => setInput(input+0) }/>
                    <Button value={'='} styleVariant={'number'} onClick={ onEqualsClick }/>
                    <Button value={'%'} styleVariant={'char'} onClick={ onPercentClick }/>
                    <Button value={'^'} styleVariant={'char'} onClick={ onPowClick }/>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: '#36485f',
        alignSelf: 'stretch',
        padding: 20,
    },
    header: {
        fontSize: 24,
        color: '#fff',
        paddingBottom: 10,
        marginBottom: 40,
        borderBottomColor: '#199187',
        borderBottomWidth: 1,
    },
    textInput: {
        alignSelf: 'stretch',
        height: 50,
        marginBottom: 100,
        color: '#fff',
        fontSize: 25,
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    buttons: {
        alignSelf: 'stretch',
        backgroundColor: '#364851',
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    exitBtn: {
        position: 'absolute',
        top: 25,
        left: 5,
    },
})

export default CalculatorScreen
