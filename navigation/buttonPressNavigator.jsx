import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator } from 'react-navigation';
import { createAppContainer } from 'react-navigation'

import RegistrationScreen from '../screens/RegistrationScreen';
import SignInScreen from '../screens/SignInScreen';
import CalculatorScreen from '../screens/CalculatorScreen';

const ButtonPressNavigator = createSwitchNavigator({
    Registration: RegistrationScreen,
    SignIn: SignInScreen,
    Calculator: CalculatorScreen,
},{
    initialRouteName: 'Registration'
})

export default createAppContainer(ButtonPressNavigator)
